import {getProject} from "@theatre/core"
import studio from "@theatre/studio"
import state from './theatre-state.json'

if (process.env.NODE_ENV !== 'production') {
	// initialize the studio so the editing tools will show up on the screen
	studio.initialize();
}
const proj = getProject("First project", {state});
const sheet = proj.sheet("Scene");
const obj = sheet.object(
	"box",
	{
	  x: 0,
	  y: 0,
	  scale: 1
	}
);

const box = document.getElementById('box');
obj.onValuesChange(({x, y, scale}) => {
	console.log("abc");
	box.style.transform = `translate(${x}px, ${y}px)`;
});

box.addEventListener('click', () => {
	sheet.sequence.play({rate: Math.random() * 9 + 1});
});
